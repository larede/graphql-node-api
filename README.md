Criando API's com Node.js, GraphQL, JWT, Sequelize e TS
=========
https://www.udemy.com/criando-apis-com-nodejs-graphql-jwt-e-sequelize

Ubuntu 16.04

Requirements
------------

- Nodejs and npm
    - $ sudo apt-get update
    - $ sudo apt-get install build-essential libssl-dev
    - $ sudo apt-get install curl
    - $ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
    - $ sudo apt-get update
    - $ nvm install v8.6.0
    - $ npm i -g npm@5.5.1

- Mysql
    - $ wget https://dev.mysql.com/get/mysql-apt-config_0.8.8-1_all.deb
    - $ sudo dpkg -i mysql-apt-config_0.8.8-1_all.deb
        - MySQL Server and Cluster
        - mysql-5.7
    - $ sudo apt-get update
    - $ sudo apt-get install mysql-server
        - pass: arede00
    - $ sudo mysql_secure_installation
        - answers: No; No; Y; Y; Y; Y

Running
------------

- Clone this project
- Open two terminals in the cloned project directory
    - $ npm run gulp
    - $ npm run dev

Testing
------------

- http://localhost:3000/graphql
    - List users

        query {
            users {
                name,
                email
            }
        }
    
    - Create an user

        mutation {
            createUser(input: {
                name:"arede", 
                email:"arede@email.com", 
                password:"arede00" 
            }) {
                id,
                name,
                email
            }
        }

    - List users (again)

        query {
            users {
                id,
                name,
                email
            }
        }
